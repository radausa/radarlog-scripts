$text = "| tx peak meas | tx peak min | status |" 
#$text = "asdf" 
$testerror = "Error"
$freqmatch = 0
$faildead = 0
$step = $args[0]
$time = Get-Date -UFormat "%A %m/%d/%Y %R %Z"
#$step = "C:\auto\[30.07.21][09'48'44][AD327200-10]-[U0399].txt"

#UPDATE THIS LINE FOR NEW SERVER FOR WHERE ALL SOURCE FILES ARE
$thepath = "C:\auto\"
####

$echolog = $thepath + "echolog.txt"
echo ( $time + ":  Starting: " + $step ) >> $echolog
$testfile = $args[0]
$iferror = $null

$dataloc = $thepath + "DataParser.exe"
$uploadloc = $thepath + "upload_to_aws.bat"

$unwrap = $thepath + "unwrap.txt"

##
##file cannot be scanned until it at least has text in it - the logger buffers so typically takes 2-3 minutes before it works
##this waits for file to have anything in it, then start the scanner
##

$ready = 0
$readycount = 0

Do {

if ((Get-Item -LiteralPath $step).length -gt 0kb) 

{ 

$ready = 1 

echo ($time + ": Getting $step length " + (Get-Item -LiteralPath $step).length) >> $echolog



}

else 

{ 

echo ("Waiting for file to get initial data to watch... Attempt number: " + $readycount)

echo ($time + ": Getting $step length " + (Get-Item -LiteralPath $step).length) >> $echolog

$readycount += 1

}



sleep 5

} 

Until ($ready -gt 0)

##start searching for the keyword

Do {

$faildead += 1

$freq = Select-String -LiteralPath $step -Pattern $text -SimpleMatch
$iferror = Select-String -LiteralPath $step -Pattern $testerror -SimpleMatch


if ($freq -ne $null)
{
    echo "FOUND KEYWORD $text"
    $freqmatch = 1

    echo ($time + ": Keyword found after (seconds) : " + $faildead ) >> $echolog

    ##copy the current data once keyword is found and copy the data     

    Copy-Item -LiteralPath $step $unwrap
    
    $unwrapfinal = $thepath + "unwrapfinal.txt"

    $grabdata = Get-Content $unwrap

    $stophere = $text
    $ctr = 0
        Do
        {
            $grabdata[$ctr++] >> $unwrapfinal #Prints data line and increments counter
        } Until ($grabdata[$ctr] -eq $stophere)

    $cleantext = ($thepath + "cleantext.txt")
    $cleantextx = ($thepath + "cleantextx.txt")


    Copy-Item -LiteralPath $unwrapfinal $cleantext


    Get-Content $cleantext | Select-Object -SkipLast 7 | Set-Content $cleantextx -Encoding UTF8

    start $dataloc $cleantextx -Wait -NoNewWindow

    start $uploadloc -Wait -NoNewWindow

    del ($thepath + "*.csv")

    del $unwrap
    del $unwrapfinal
    del $cleantext
    del $cleantextx

    Break
}
else
{

  if ($iferror -ne $null)

            {
            
            echo ($time + ": ERROR FOUND, BREAKING" + $testfile ) >> $echolog

            Break
            
            }



            if ($faildead -eq 1080)
            {    
            

            echo ($time + ": Keyword has not been found in over 90 minutes, ending.") >> $echolog
            Break

            }
   echo ("Keyword not found - attempt number: " + $faildead ) >> $echolog
   echo $faildead
   
   Start-Sleep -Seconds 5
   
}      

}

Until ($freqmatch -eq 1)
# SIG # Begin signature block
# MIIFcAYJKoZIhvcNAQcCoIIFYTCCBV0CAQExCzAJBgUrDgMCGgUAMGkGCisGAQQB
# gjcCAQSgWzBZMDQGCisGAQQBgjcCAR4wJgIDAQAABBAfzDtgWUsITrck0sYpfvNR
# AgEAAgEAAgEAAgEAAgEAMCEwCQYFKw4DAhoFAAQUeCGqCzI+EWLO9hLmFPeapwIO
# YxegggMKMIIDBjCCAe6gAwIBAgIQK/Ic1NF6r4BFHszPJhUUbzANBgkqhkiG9w0B
# AQsFADAbMRkwFwYDVQQDDBBBVEEgQXV0aGVudGljb2RlMB4XDTIxMDgwNDIwMDcx
# MFoXDTIyMDgwNDIwMjcxMFowGzEZMBcGA1UEAwwQQVRBIEF1dGhlbnRpY29kZTCC
# ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALhxbrVRkNjRBRR9VHYt/J6t
# lDrLwVr3ktjpczCisyytYWlJWwPqWb+q7hJTo+ikj87GvQMiF8Qh1k693XFsumLu
# s2ZjSnKDmgc1tnAi6seHe2k9co4Sa7Cj2eJ5HeqxVKtfHfhVIhIRg37LnsqrD8fd
# snKgKBcoBWVngyuXh2t2876QwGNvXO7RnhYw++Wz6a1UlkXzxL9iVF2xXTmYy3or
# PVXBRpuPUVc9sLX4/pwU5LqDCWc8WZvIDRalXiQeRhmfBktQSctlZ4QjDkTQTMoO
# +6VO5LTiXyVyy/Atvt5RB/zcbTblDUGjGyZtjytIsN1EsnaI3m9FvorrTXzK9wkC
# AwEAAaNGMEQwDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoGCCsGAQUFBwMDMB0G
# A1UdDgQWBBQvi87iT2et47K7CdehLAzB5qFPDzANBgkqhkiG9w0BAQsFAAOCAQEA
# iU0dCGNWenYQA2/BYPG5SKq9R8dco4Q9MpcnxIW/YCmDYSsuDScOtc93gbzbyv1s
# MCWi+CHyDFnAPJU7nBA2+aJ+zjL+/IbpUmdcl/XNC/0ybkU8D4+2Flm0NwsQcid2
# qHw4MR+VaURIcFKlPTJYbgKfIiX4vfdZpAO/zQHouWyDchpouDB8kcxIVS4ABTPn
# ZvBaBHV0uaSIl19mJ+SeoieX1LOEA7te9svWnWQ0uFVccUi1Izco8NiMQN6hng1D
# YGNKex8rfaRr3gcb1kctB+wltwaSXZYHngDwIquH8H78MwU9zX7hnvi5It8CjL0k
# lfnbHCNOoBPCs01pjnf+4jGCAdAwggHMAgEBMC8wGzEZMBcGA1UEAwwQQVRBIEF1
# dGhlbnRpY29kZQIQK/Ic1NF6r4BFHszPJhUUbzAJBgUrDgMCGgUAoHgwGAYKKwYB
# BAGCNwIBDDEKMAigAoAAoQKAADAZBgkqhkiG9w0BCQMxDAYKKwYBBAGCNwIBBDAc
# BgorBgEEAYI3AgELMQ4wDAYKKwYBBAGCNwIBFTAjBgkqhkiG9w0BCQQxFgQUPKbq
# as/DgfScryBOVmvsvZdb9+UwDQYJKoZIhvcNAQEBBQAEggEAaC8B17TwEjqDqzEw
# 4V4VzA8cP9zH0SuGDxmVekvvOfYSvvXFXcgHN681h79sSS7rnh7vn0bRrrTN+Euz
# YGZSiOtpOKTRqfCk+saPgtiSRtxmzP03nofTcSekAt5y5p4Cx0oVg9Twe769sVm9
# jMzWfMqr0jydnnWWlfc/bzqI2cDW8Kkx9T6otCl8YNVLBALKo5TD+XqmMVe+HgJC
# RwU/v6kAG3yGp++Fi0YRJ+A33rarFH5+Zkma/evBAg4C7/Z4iE6zJgap4Lu4l14U
# sCc2UDAr/ZwEImV3Kmhp/l2eFcz4zVVrkcMBmOEoAgssFaIKrSebzIizLbqpL5Fv
# QbulUw==
# SIG # End signature block
