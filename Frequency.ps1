###################################################################################################################################
### CREATED BY REED TEWELL : RADA USA       #######################################################################################
### PURPOSE IS TO AUTOMATICALLY CAPTURE OUTPUT FROM REI ANECHOIC CHAMBER LOG DATA AND OUTPUT TO A DATABASE IN PARSEABLE FORMAT ####
###################################################################################################################################

######################
### FILE STRUCTURE ###
$root = "C:\auto\"
#$step = ($root+"a.txt")
$step = $args[0]
$holdfile = ($root+"holdfile.txt")
$dataloc = ($root+"DataParser.exe")
$futter = ($root+"pgfutter.exe")
######################

###############################################
### PRIMARY FREQUENCY "BLOCK" SEARCH WINDOW ###
#$searchfreqs = [System.Collections.Generic.List[System.Object]]("~","| frequency set | 1 |","| frequency set |            2             |","| frequency set |            3             |","| frequency set |            4             |","| frequency set |            5             |","| frequency set |            6             |","| frequency set |            7             |","| frequency set |            8             |","| frequency set |            9             |")
$searchfreqs = [System.Collections.Generic.List[System.Object]]("~","1","2","3","4","5","6","7","8","9")

$searchafter="| tx peak meas | tx peak min | status |"
$finalfreqs = [System.Collections.Generic.List[System.Object]]("frequency set")
###############################################

###UPLOAD PARAMS###
$uploadtables = ("patchesrxtest","patchesrxtestch","patchestxtest","patchestxtestch","patchtxcurrentcalib","patchtxcurrentcalibsetup","radartxfinaltest","radartxfinaltest2","radartxfinaltestfinal")
$uploaddocs = ("PatchesRxTest.csv","PatchesRxTestCh.csv","PatchesTxTest.csv","PatchesTxTestCh.csv","PatchTxCurrentCalib.csv","PatchTxCurrentCalibSetup.csv","RadarTxFinalTest.csv","RadarTxFinalTest2.csv","RadarTxFinalTestFinal.csv")
$batchaws = $root + "upload_to_aws.bat"
$testbatchaws = $root + "test_upload_to_aws.bat"
#######################
#### EXIT DETECTION ###
$exitchecking = [System.Collections.Generic.List[System.Object]]("close log","ERROR","TIME OUT set_bias_read_curr","KeyboardInterrupt")
$exitfound = 0
$currenttest = 0
$currentIndex = 0
$runcount = 0
$errored = 0
#######################

#### ERROR CATCHING ####
$tempcounter = 0

###############################
##### FILE SIZE DETECTION #####
###############################
$currentsize = 0
$firstrun = 0


###TRACE FREQUENCY STATUS###
$freqstatus = 'DEBUG RUN'
$currentfreq = '0'
$MetaSQL = ($root+'SQL/uploadmetadata.sql')
$TestMetaSQL = ($root+'SQL/uploadmetadata_test.sql')
$UpdateSQL = ($root+'SQL/updatesql.sql')
$TestUpdateSQL = ($root+'SQL/updatesql_test.sql')
$UpdateFailuresSQL = ($root+'SQL/updatefailures')
$metafile = ($root+'RadarMetadata.csv')

#############################################################################
#### SMS ALERT ##############################################################
$url = "https://api.twilio.com/2010-04-01/Accounts/AC36e592350b0093d8fe02c37cd4017e76/Messages.json"
$sid = "AC36e592350b0093d8fe02c37cd4017e76"
$token = "ac557c0a5f5651b53d75c7978be3ff21"
$number = "+16106097984"
$p = $token | ConvertTo-SecureString -asPlainText -Force
$credential = New-Object System.Management.Automation.PSCredential($sid, $p)
$testphones = Import-Csv ($root+"phones\testphones.csv") | select Phone,Name
$debugphones = Import-Csv ($root+"phones\debugphones.csv") | select Phone,Name

### send sms to debug team, test team, and a flag for whetehr or not to send the initial test (good for renabling non-exit tests)
$phonesyesorno = Import-Csv ($root+"phones\textmessages_on_off.csv") | select Phone,Name ## lets SMS settings change without adjusting code
$sendtodebug = 1
$sendtotest = 1
$alertstart = 1
$allowalltest = 1
##############################################################################

function Get-AlertSMS{
Param ($Text)

$phonesyesorno = Import-Csv ($root+"phones\textmessages_on_off.csv") | select name,onoff
$Text = $Text[0..256] -join ""

$testphones = Import-Csv ($root+"phones\testphones.csv") | select Phone,Name
$debugphones = Import-Csv ($root+"phones\debugphones.csv") | select Phone,Name

$sendtodebug = [int]$phonesyesorno[0].onoff
$sendtotest = [int]$phonesyesorno[1].onoff
$alertstart = 1
$allowalltest = [int]$phonesyesorno[2].onoff

         if ($sendtodebug -eq 1){
            for ($para = 0;$para -le (($debugphones.Phone).Count -1); $para+=1)
            {
                 $params = @{ To = $debugphones[$para].Phone; From = $number; Body = $Text }
                 Invoke-WebRequest $url -Method Post -Credential $credential -Body $params -UseBasicParsing |
                 ConvertFrom-Json | Select sid, body
            }}

            if($allowalltest -eq 1){
        if ($sendtotest -eq 1){
            for ($para = 0;$para -le (($testphones.Phone).Count -1); $para+=1)
            {
                 $params = @{ To = $testphones[$para].Phone; From = $number; Body = $Text }
                 Invoke-WebRequest $url -Method Post -Credential $credential -Body $params -UseBasicParsing |
                 ConvertFrom-Json | Select sid, body
            }}}

}


### CLEAR ALL JUNK DATA ##
del ($root + "*.csv")

echo ("MATCH COUNT = " + $matchcount)
echo ("START ARRAY TOTAL: " + $searchfreqs.Count)

##################################################################
## EVERY 5 SECONDS, LOOK FOR FILE CHANGES, OTHERWISE DO NOTHING ##
##################################################################

do {

    $runcount += 1
    echo $runcount
    echo ("Temp Status: " + $tempcounter)

    if (($runcount -gt 2160)) 
    {

    $errored =1
    
    Get-AlertSMS ("The log trace for " + $metadata.SerialNumber + " (" + $metadata.Model + ")" + " has been running for 3 hours without any new information. Stopping log trace.")

            Break
    
    }


    ### THIS RUNS IF FILE SIZE DIFFERS FROM PREVIOUS ###
    if ($currentsize -ne (Get-Item -LiteralPath $step).length)

    {

        

        #### IF TEST HAS BEEN RUNNING FOR 20 MINUTES WITHOUT A FREQUENCY CONFIRM, ALERT ####
        if (($runcount -gt 240) -and ($tempcounter -eq 0)) 
        
        {
        # Twilio API endpoint and POST params

        ### CHECK TO SEE IF MESSAGES ARE ENABLED FOR DEBUG TEAM, THEN LOOP THROUGH NUMBERS AND SEND

            $erorred = 1

            Get-AlertSMS ("The Anechoic test for " + $metadata.SerialNumber + " (" + $metadata.Model + ")" + " has been running for 20 minutes without a frequency test. Stopping live log.")

            Break

            }
        
           
        $currentsize = (Get-Item -LiteralPath $step).length
        echo ("CURRENT SIZE: " + $curentsize)


        #### GET NEW DATA, BUILD THE MATCHES AND MATCH COUNTS ####
        Copy-Item -LiteralPath $step $holdfile
        $findMatches = Get-Content -LiteralPath $holdfile | select-string -Pattern 'frequency set' -SimpleMatch | select-string -Pattern $searchfreqs -SimpleMatch
        $exitMatch = Get-Content -LiteralPath $holdfile | select-string -Pattern $exitchecking -SimpleMatch
        $matchcount = [int]$findMatches.Count
        ##########################################################


        ### CHECK TO SEE IF AN EXIT CONDITIONS WERE FOUND ###
        if ($exitMatch.Count -gt 0)
        {

            ### HAS METADATA BEEN CAPTURED YET? (CHECK FOR TESTING HISTORICAL) ###
                    if ($firstrun -eq 0)
            {
        
            $firstrun = 1

            Start-Process -FilePath $dataloc -ArgumentList "-o m -f $holdfile" -Wait -NoNewWindow

            echo "ETRACTING METADATA FOR FIRST UPLOAD BEFORE EXIT"

            Sleep 1

             #### READ SERIAL NUMBER ####
            $metadata = Import-Csv -LiteralPath $metafile | select SerialNumber,Model
            echo $metadata.SerialNumber
            $finalmatchcount = Get-Content -LiteralPath $holdfile | select-string -Pattern $finalfreqs -SimpleMatch
            ############################

            #Start-Process -FilePath $futter -ArgumentList "--host 18.253.88.166 --port 5432 --db RADAR --schema public --table metadata --user radausa --pw euthvkoO1jENLVtUS9e4 csv RadarMetadata.csv"

            $MetaSQL = 'C:/auto/SQL/uploadmetadata.sql'

            $sqlmetainfo = Import-Csv $metafile | select SerialNumber,HostIP,INTimeIP,TapVersion,SWVersion,RFMVersion,Model,DateTime,DateTimeConverted,UniqueID,PartNumber

            ##grab all CSV data and set it, because the PSQL call does not like pulling object indexes
            $v1 = $sqlmetainfo[0].SerialNumber
            $v2 = $sqlmetainfo[0].HostIP
            $v3 = $sqlmetainfo[0].INTimeIP
            $v4 = $sqlmetainfo[0].TapVersion
            $v5 = $sqlmetainfo[0].SWVersion
            $v6 = $sqlmetainfo[0].RFMVersion
            $v7 = $sqlmetainfo[0].Model
            $v8 = $sqlmetainfo[0].DateTime
            $v9 = $sqlmetainfo[0].DateTimeConverted
            $v10 = $sqlmetainfo[0].UniqueID
            $v11 = $freqstatus
            $v12 = $currentfreq

            ###POSTGRE MUST BE INSTALLED FOR PSQL TO BE HERE ###
            Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
            $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
            $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -v v5="'$v5'" -v v6="'$v6'" -v v7="'$v7'" -v v8="'$v8'" -v v9="'$v9'" -v v10="'$v10'" -v v11="'$v11'" -v v12="'$v12'" -h 18.253.88.166 -U radausa -d RADAR -f $MetaSQL
            $workingid = $results[2].Trim()
            #################################################TEST DATA#########################
            $v13 = $sqlmetainfo[0].PartNumber

            $testresults = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -v v5="'$v5'" -v v6="'$v6'" -v v7="'$v7'" -v v8="'$v13'" -v v9="'$v8'" -v v10="'$v9'" -v v11="'$v10'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestMetaSQL
            $testworkingid = $testresults[2].Trim()

            $v3 = $testworkingid
                     $results = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL
            ##########################################################################
            echo "ID: $workingid"
            echo "ID: $testworkingid"

            Set-Location 'C:\auto\';



            ###



            ###

            
            sleep 1   
        
            }

            $exitfound = 1;

            echo ("EXITING DUE TO " + $exitMatch + $exitMatch.Count)

           
           ## MATCH A TRUE COMPLETE TEST ""
            if ([string]$exitMatch -eq "close log")
            { 
                if([int]$finalmatchcount.Count -eq 0)
                {
                    $exitMatch = "COMMUNICATION FAILURE"
                    $comm = 1
                    $erorred = 1

                }

                else
                {
                $exitMatch = "COMPLETED TEST"

                }

                                if ($countfails.Count -gt 0) {$v1 = 'FAILED'} else {$v1 = "PASSED"}
                                if ($comm -eq 1) {$v1 = "COMM. FAILURE"}
                                $v2 = $currentIndex
                                $v3 = $workingid
                                $v4 = $countfails.Count
                                Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
                                $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
                                $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -h 18.253.88.166 -U radausa -d RADAR -f $UpdateSQL

                                $v3 = $testworkingid
                                $testresults = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL

                                Set-Location 'C:\auto\';
            }

            #### READ SERIAL NUMBER ####
            $metadata = Import-Csv -LiteralPath $metafile | select SerialNumber,Model
            echo $metadata.SerialNumber
            $finalmatchcount = Get-Content -LiteralPath $holdfile | select-string -Pattern $finalfreqs -SimpleMatch
            ############################

            sleep 1
    

         Get-AlertSMS ("The Anechoic test for " + $metadata.SerialNumber + " (" + $metadata.Model + ")" + " has completed with code: " + $exitMatch + ". There were " + $finalmatchcount.Count + " detected frequency tests.")

         if ([string]$exitmatch -match "[Errno 10060]")

         {
           # Get-AlertSMS ("Error number 10060 Troubleshooting: Check Spectrum Analyzer on rear of rack.")
         }


         $errormatch = Get-Content -LiteralPath $holdfile | select-string -Pattern $exitchecking -SimpleMatch
         ##Jump out if it's an error
         if ([string]$errormatch -like "*ERROR*"){
                     $v1 = "ERRORED"
                     $v2 = $currentIndex
                     $v3 = $workingid
                     $countfails= Get-Content -LiteralPath $holdfile | select-string -Pattern 'fail' -SimpleMatch
                     $v4 = $countfails.Count
                     Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
                     $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
                     $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -h 18.253.88.166 -U radausa -d RADAR -f $UpdateSQL
                    

                     $v3 = $testworkingid
                     $testresults = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL

                     Set-Location 'C:\auto\';

                     $errored = 1
         Break
         
         
         }
          


        }

        #### IF IT'S THE FIRST RUN THROUGH, PULL THE METADATA #####
        if ($firstrun -eq 0)
        {


        
        $firstrun = 1
     
        Start-Process -FilePath $dataloc -ArgumentList "-o m -f $holdfile" -Wait -NoNewWindow

        


            $sqlmetainfo = Import-Csv $metafile | select SerialNumber,HostIP,INTimeIP,TapVersion,SWVersion,RFMVersion,Model,DateTime,DateTimeConverted,UniqueID,PartNumber

            ##grab all CSV data and set it, because the PSQL call does not like pulling object indexes
            $v1 = $sqlmetainfo[0].SerialNumber
            $v2 = $sqlmetainfo[0].HostIP
            $v3 = $sqlmetainfo[0].INTimeIP
            $v4 = $sqlmetainfo[0].TapVersion
            $v5 = $sqlmetainfo[0].SWVersion
            $v6 = $sqlmetainfo[0].RFMVersion
            $v7 = $sqlmetainfo[0].Model
            $v8 = $sqlmetainfo[0].DateTime
            $v9 = $sqlmetainfo[0].DateTimeConverted
            $v10 = $sqlmetainfo[0].UniqueID
            $v11 = "INTIALIZING"
            $v12 = $currentfreq

            Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
            $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
            $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -v v5="'$v5'" -v v6="'$v6'" -v v7="'$v7'" -v v8="'$v8'" -v v9="'$v9'" -v v10="'$v10'" -v v11="'$v11'" -v v12="'$v12'" -h 18.253.88.166 -U radausa -d RADAR -f $MetaSQL
            $workingid = $results[2].Trim()

             $v13 = $sqlmetainfo[0].PartNumber

            $testresults = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -v v5="'$v5'" -v v6="'$v6'" -v v7="'$v7'" -v v8="'$v13'" -v v9="'$v8'" -v v10="'$v9'" -v v11="'$v10'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestMetaSQL
            $testworkingid = $testresults[2].Trim()

            $v3 = $testworkingid
                     $results = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL
            ##########################################################################
            echo "ID: $workingid"
            echo "ID: $testworkingid"

            echo "ID: $workingid"

            Set-Location 'C:\auto\';
     
     
        #Start-Process -FilePath $futter -ArgumentList "--host 18.253.88.166 --port 5432 --db RADAR --schema public --table metadata --user radausa --pw euthvkoO1jENLVtUS9e4 csv $metafile"

                    echo "ETRACTING METADATA"

             #### READ SERIAL NUMBER ####
            $metadata = Import-Csv -LiteralPath $metafile | select SerialNumber,Model
            echo $metadata.SerialNumber
            $finalmatchcount = Get-Content -LiteralPath $holdfile | select-string -Pattern $finalfreqs -SimpleMatch
            ############################  

                if ($alertstart -eq 1 ){
                Get-AlertSMS ("The Anechoic test for " + $metadata.SerialNumber + " (" + $metadata.Model + ")" + " has started.")
                }
            

                  
            
               
        
        }


################################################################################
### LOOP THROUGH ALL FOUND MATCHES IN THE FILE AND LOOK FOR COMPLETED BLOCKS ###
### WHICH THEN REMOVES THAT SEARCH TERM FROM THE PRIMARY ARRAY #################
################################################################################

        for ($line = 0; $line -le ($matchcount -1); $line += 1) 



       {
        echo "###########################"
        echo "LINE $line"
        $tempcounter = 1

            $linenumber= Get-Content -LiteralPath $holdfile | select-string -Pattern 'frequency set' -SimpleMatch | select-string -Pattern $searchfreqs -SimpleMatch
            $countfails= Get-Content -LiteralPath $holdfile | select-string -Pattern 'fail' -SimpleMatch


            echo "DETECTED INFORMATION" $linenumber


            #if ($linenumber -match $searchfreqs[1]) {echo ("FOUND THIS: " + $searchfreqs.IndexOf([string]$linenumber[0]))}

            $remedy = [string]$linenumber[0]
            $remedy = $remedy -replace '\s',''
            $remedy = $remedy.substring(14,1)
            
            echo "CATCH FIRST RESULT" $remedy
            $currentIndex = $searchfreqs.IndexOf([string]$remedy)

            echo "INDEX: " $currentIndex

            if (($linenumber.Count -ne 0) -and ([int]$matchcount -ne 0)) {

                    ###
                    echo ("MATCHED LINES: $linenumber")
                    echo ("MATCHED ITEMS COUNT: " + $linenumber.Count)
                    echo ("SEARCH FREQUENCIES: " + $searchfreqs)
                    ###

                    $matchedLine = $linenumber[0].LineNumber

                    echo ("CURRENT SEARCH LINE: " + $linenumber[0])

                    echo ("TESTING " + $currentIndex)

                    $lineafter = Get-Content -LiteralPath $holdfile | select-string -Pattern $searchafter -SimpleMatch

                    ## for every END RESULT it found, see if that line is GREATER than the currently searched keyword

                    foreach ($match in $lineafter.LineNumber)
                    {

                    if ([int]$match -gt [int]$matchedLine)

                        {

                            

                            Start-Process -FilePath $dataloc -ArgumentList "-o $currentIndex -f $holdfile" -Wait -NoNewWindow

                            #for ($find = 0; $find -le (9-1); $find += 1) 
                            #{
                            #echo ("Uploading " + $uploadtables[$find])
                            #Start-Process -FilePath $futter -ArgumentList "--host 18.253.88.166 --port 5432 --db RADAR --schema public --table $uploadtables[$find] --user radausa --pw euthvkoO1jENLVtUS9e4 csv $uploaddocs[$find]" -Wait -NoNewWindow
                            #}

                            sleep 2

                            start $batchaws -Wait -NoNewWindow

                            start $testbatchaws -Wait -NoNewWindow

                            ##UPDATE STATUS

                            if ($countfails.Count -gt 0) {$v1 = 'FAILING'} else {$v1 = "IN PROGRESS"}
                                
                                $v2 = $currentIndex
                                $v3 = $workingid
                                $v4 = $countfails.Count
                                Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
                                $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
                                $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -h 18.253.88.166 -U radausa -d RADAR -f $UpdateSQL

                                $v3 = $testworkingid
                                $results = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL

                                Set-Location 'C:\auto\';
                            
                            echo $matchedLine
                            echo $match
                            echo ('META ID: ' + $workingid)
                            echo ('TESTMETA ID: ' + $testworkingid)

                            $copy = Get-Content -LiteralPath $holdfile

                            Get-Content -Path $holdfile | Select -Index ($matchedLine..$match) | Set-Content -Path "C:\auto\failcount.txt"

                            Sleep 1

                            $failblock = Get-Content C:\auto\failcount.txt | select-string -Pattern 'fail' -SimpleMatch

                            ### BUILD COLUMN STRING
                            $v2 = $failblock.Count
                            $v3 = $workingid
                            Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
                            $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
                            $results = & .\psql -v v2="'$v2'" -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADAR -f ($UpdateFailuresSQL+$currentIndex+".sql")

                            Set-Location 'C:\auto\';

                            echo ("NUMBER OF FAILURES ON FREQUENCY $currentIndex " + " : " +$failblock.Count)
                
                            echo ("FOUND " + $linenumber[0] + " ON $matchedLine and a END on $match")

                            ##rip out matching list item
                            #$searchfreqs.RemoveAt($line)
                            echo ("CURRENT LINE: " + $matchedLine)
                            $searchfreqs[$currentIndex] = "~"
                            #$line -= 1
                            sleep 5
                            del ($root + "*.csv")
                            Break

                        }

                        else

                        {
    
                            echo "NOTHING FOUND ON LINE $match"

                        }


                    }



                    }

        echo ("END TOTAL ARRAY: " + $searchfreqs.Count)
        echo ("LINE: " +$line)
        echo ("MATCH COUNT = " + $matchcount)
        echo "###########################"

        }

        Sleep 5

        #end size check
        } 

    else 
    
    {
        echo "SAME FILE SIZE, NO ACTION"
        sleep 5
    }
       
    } Until ($exitfound -eq 1)


    ## ONE FINAL UPDATE DEPENDING ON STATUS

   

   if ($errored -eq 0){
                
   if ($countfails.Count -gt 0) {$v1 = 'FAILED'} else {$v1 = "PASSED"}
   if([int]$finalmatchcount.Count -eq 0)
   {
      $v1 = "COMM. FAILURE"
   } }

   else {$v1 = "ERRORED"}

   $v2 = $currentIndex
   $v3 = $workingid
   $v4 = $countfails.Count
   Set-Location 'C:\Program Files\PostgreSQL\13\bin\';
   $env:PGPASSWORD = 'euthvkoO1jENLVtUS9e4';
   $results = & .\psql -v v1="'$v1'" -v v2="'$v2'" -v v3="'$v3'" -v v4="'$v4'" -h 18.253.88.166 -U radausa -d RADAR -f $UpdateSQL

   $v3 = $testworkingid
   $results = & .\psql -v v3="'$v3'" -h 18.253.88.166 -U radausa -d RADARTEST -f $TestUpdateSQL

   Set-Location 'C:\auto\';

   C:\auto\pscp.exe -sftp -pw Ho\5g]@S9: $step 'anechoic@file.radausa.org:/Anechoic Data/Completed Chamber Tests/'